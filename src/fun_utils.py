from pandas import read_csv
import numpy as np
import matplotlib.pyplot as plt


def load_data(filename):
    """
    Load data from a csv file

    Parameters
    ----------
    filename : string
        Filename to be loaded.

    Returns
    -------
    X : ndarray
        the data matrix.

    y : ndarray
        the labels of each sample.
    """
    data = read_csv(filename)
    z = np.array(data)
    y = z[:, 0]
    X = z[:, 1:]
    return X, y


def split_data(features, labels, tr_fraction=0.5):
    """
    Split the data x, y into two random subsets

    """
    # creo un vettore da 0 a size per indicizzare i lavori che mi interessano
    random_vector = np.linspace(0,
                                labels.shape[0],
                                labels.shape[0],
                                endpoint=False,
                                dtype=int)

    # randomizzo i valori
    np.random.shuffle(random_vector)

    print("random_vector: ", random_vector.shape)
    size_training = (int)(labels.size * tr_fraction)
    size_test = labels.size - size_training

    random_vector_training = random_vector[0:size_training]
    random_vector_test = random_vector[size_training:]

    data_class_training = labels[random_vector_training]
    data_feature_training = features[random_vector_training, :]
    data_class_test = labels[random_vector_test]
    data_feature_test = features[random_vector_test, :]

    return data_class_training, data_feature_training, data_class_test, data_feature_test

def plotFirst10Images(data_feature, data_classes, width, height):
    num_valori = len(data_classes)
    if num_valori > 10:
        num_valori = 10
    for i in range(0, num_valori):
        image_num = data_feature[i].reshape(width, height)
        plt.subplot(4, 6, (i + 1) * 2)
        plt.imshow(image_num)