import numpy as np
from sklearn.metrics.pairwise import euclidean_distances, pairwise_distances


class NMC(object):
    """
    Class implementing the Nearest Mean Centroid (NMC) classifier.

    This classifier estimates one centroid per class from the training data,
    and predicts the label of a never-before-seen (test) point based on its
    closest centroid.

    Attributes
    -----------------
    - centroids: read-only attribute containing the centroid values estimated
        after training

    Methods
    -----------------
    - fit(x,y) estimates centroids from the training data
    - predict(x) predicts the class labels on testing points

    """

    def __init__(self):
        self._centroids = None
        self._class_labels = None  # class labels may not be contiguous indices

    @property
    def centroids(self):
        return self._centroids

    @property
    def class_labels(self):
        return self._class_labels

    def fit(self, features, labels):

        """ FIT Trainig data: crea i centroidi """
        self._class_labels = np.unique(labels)
        num_feature = features.shape[1]

        # matrix centroids
        self._centroids = np.zeros((len(self._class_labels), num_feature))

        for i, label in enumerate(self._class_labels):
            #           features_i = data_feature[label == data_class, :]
            #           centroids[i] = features_i.mean(axis=0)
            features_i = features[label == labels, :]
            self._centroids[i] = features_i.mean(axis=0)

        return self._centroids, self._class_labels

    def predict(self, xts):
        # calcolo la distanza. ottengo una lista di features (per righe)
        # per ogni feature ho 10 elementi. ogni elemento è la distanza da 0, 1, 2,...8,9
        matrix_distance = pairwise_distances(xts, self._centroids)

        # computo il minimo tra i 10 elementi quindi lungo l'asse 1 (da sx a dx)
        vector_prediction = np.argmin(matrix_distance, axis=1)

        return vector_prediction
