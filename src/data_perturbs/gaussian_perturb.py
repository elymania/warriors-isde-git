import numpy as np

from .data_perturb import DataPerturb


class GaussianPerturb(DataPerturb):
    def __init__(self, mean, deviation=1.0):
        self._mean = mean
        self._deviation = deviation

    @property
    def deviation(self):
        return self._deviation

    @deviation.setter
    def deviation(self, value):
        self._deviation = value

    @property
    def mean(self):
        return self._mean

    @mean.setter
    def mean(self, value):
        self._mean = value

    def add_perturb(self, value):
        result = 2 * np.random.normal(self._mean, self._deviation, size=value.shape) + value

        # clip operation:
        result[result > 255] = 255
        result[result < 0] = 0

        print("result: ")
        print(result)

        return result
