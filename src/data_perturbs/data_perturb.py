from abc import ABCMeta, abstractmethod, abstractproperty


class DataPerturb(object):
    """Create a disturb on data"""
    __metaclass__ = ABCMeta

    @abstractmethod
    def add_perturb(self, value):
        raise NotImplementedError("This is an abstract class")
