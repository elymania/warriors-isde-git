import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from classifiers import NMC
from fun_utils import *
from data_perturbs import GaussianPerturb

# lista classi e lista features
matrix_feature, matrix_class = load_data("../data/mnist_data.csv")
print(matrix_class.shape)
print(matrix_feature.shape)

class_tr, feature_tr, class_ts, feature_ts = split_data(matrix_feature, matrix_class, 0.8)

clf = NMC()

centroids, labels = clf.fit(feature_tr, class_tr)


w = 28
h = 28
# plotFirst10Images(centroids, labels, w, h)
plotFirst10Images(feature_ts, class_ts, w, h)
plt.show()

disturboGaussiano = GaussianPerturb(0,30)

perturb_feature = disturboGaussiano.add_perturb(feature_ts)
plotFirst10Images(perturb_feature, class_ts, w, h)
plt.show()

print("test CI 2")



