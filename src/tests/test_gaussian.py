import unittest
import numpy as np
import pandas as pd
from os import path

from data_perturbs import *
from fun_utils import *


class TestGaussian(unittest.TestCase):
    def setUp(self):
        mnist_path = path.join(
            path.dirname(__file__), '..', '../data', 'mnist_data.csv')
        x, y = load_data(filename=mnist_path)

        # Rescale data in 0-1
        self.x = x + 255
        self.y = y
        self.gaussian = GaussianPerturb(0, 1)

    def test_add_perturb(self):
        self.gaussian.add_perturb(self.x)

        assert self.x.all() < 255

